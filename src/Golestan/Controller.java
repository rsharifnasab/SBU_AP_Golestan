package Golestan;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import static Golestan.peopleType.*;

public class Controller {
    @FXML
    private  Button goToSignUp;

    @FXML
    private Button LoginButton;

    @FXML
    private Label errorLabel;

    @FXML
    private TextField userNameText;

    @FXML
    private PasswordField passWordText;

    public void login(ActionEvent event) throws Exception {
        String username = userNameText.getText();
        String password = passWordText.getText();
        peopleType access = Model.checkCredential(username,password);
        if(access == NONE){
            errorLabel.setVisible(true);
            return;
        }
        errorLabel.setVisible(false);
        System.out.println("ok you are in!\nyou are a "+ access.toString());
        String sceneFile = access.toString() + ".fxml";
        String state = access.toString().toLowerCase();
        Stage primaryStage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource(sceneFile));
        primaryStage.setTitle(state + " Page!");
        primaryStage.setScene(new Scene(root, 1024 , 768));
        primaryStage.show();
    }

}
