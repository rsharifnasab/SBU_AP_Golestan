package Golestan;
import java.io.*;
import java.util.*;

import static Golestan.peopleType.*;

public class Model {
    public static peopleType checkCredential(String username, String password){
        if(username.length()== 0 || password.length() == 0) return NONE;
        try {
            File adminFile = new File("./passwordDB/admin.txt");
            Scanner UPRead = new Scanner(adminFile);
            while (UPRead.hasNextLine()) {
                String data[] = UPRead.nextLine().split(" ");
                if(data[0].equals(username) && data[1].equals(password)) return ADMIN;
            }


            File teacherFile = new File("./passwordDB/teacher.txt");
            UPRead = new Scanner(teacherFile);
            while (UPRead.hasNextLine()) {
                String data[] = UPRead.nextLine().split(" ");
                if(data[0].equals(username) && data[1].equals(password)) return TEACHER;
            }

            File studentFile = new File("./passwordDB/student.txt");
            UPRead = new Scanner(studentFile);
            while (UPRead.hasNextLine()) {
                String data[] = UPRead.nextLine().split(" ");
                if(data[0].equals(username) && data[1].equals(password)) return STUDENT;
            }
            UPRead.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            System.out.println(e);//e.printStackTrace();
        }
        return NONE;
    }



}
